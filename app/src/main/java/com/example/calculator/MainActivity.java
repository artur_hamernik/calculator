package com.example.calculator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String TEXT_VIEW = "text_view";
    private static final String TEXT_VIEW_UP = "text_view_up";
    private static final String DIGIT = "digit";
    private static final String PROCESS = "process";
    private static final String NO_ZERO = "Nie wolno dzielic przez zero !!!";

    Button btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btnAC, btnPM, btnMulti, btnDiv, btnPercent, btnMinus, btnPlus, btnEquals, btnFraction, btnLog, btnSil, btnSQRT, btnPow3, btnPow2;
    TextView textView, textViewUp;
    Double digit1 = null, digit2 = null;
    private int process = 5;
    boolean start = true;


    private void operation() {
        switch (process) {
            case 1:
                digit1 = digit2 + Double.parseDouble((String) textView.getText());
                textView.setText(String.valueOf(digit1));
                break;
            case 2:
                digit1 = digit2 - Double.parseDouble((String) textView.getText());
                textView.setText(String.valueOf(digit1));
                break;

            case 3:
                digit1 = digit2 * Double.parseDouble((String) textView.getText());
                textView.setText(String.valueOf(digit1));
                break;

            case 4:
                if (Double.parseDouble((String) textView.getText()) == 0) {
                    textViewUp.setText(NO_ZERO);
                } else {
                    digit1 = digit2 / Double.parseDouble((String) textView.getText());
                    textView.setText(String.valueOf(digit1));
                }
                break;
            case 5:
                String temp = textView.getText() + " = ";
                textViewUp.setText(temp);
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSil = findViewById(R.id.btnSilnia);
        btnSQRT = findViewById(R.id.btnSQRT);
        btnLog = findViewById(R.id.btnLog);
        btnPow2 = findViewById(R.id.btnPow2);
        btnPow3 = findViewById(R.id.btnPow3);
        btn0 = findViewById(R.id.btn0);
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        btn4 = findViewById(R.id.btn4);
        btn5 = findViewById(R.id.btn5);
        btn6 = findViewById(R.id.btn6);
        btn7 = findViewById(R.id.btn7);
        btn8 = findViewById(R.id.btn8);
        btn9 = findViewById(R.id.btn9);
        btnAC = findViewById(R.id.btnAC);
        btnPM = findViewById(R.id.btnPM);
        btnMulti = findViewById(R.id.btnMulti);
        btnDiv = findViewById(R.id.btnDiv);
        btnPercent = findViewById(R.id.btnPerc);
        btnMinus = findViewById(R.id.btnMin);
        btnPlus = findViewById(R.id.btnPlus);
        btnEquals = findViewById(R.id.btnEquals);
        btnFraction = findViewById(R.id.btnFrac);
        btnLog = findViewById(R.id.btnLog);
        btnSil = findViewById(R.id.btnSilnia);
        btnSQRT = findViewById(R.id.btnSQRT);
        btnPow3 = findViewById(R.id.btnPow3);
        btnPow2 = findViewById(R.id.btnPow2);
        textView = findViewById(R.id.textView);
        textViewUp = findViewById(R.id.textViewUp);

        portraitButtons();
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            landscapeButtons();
        }
    }
    private void portraitButtons(){
        textView.setText("0");
        btnAC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText("0");
                start = true;
                textViewUp.setText(null);
                unBlock();
                unBlockFraction();
            }
        });

        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(start){
                    textView.setText("0");
                    start = false;
                }else {
                    String temp = textView.getText() + "0";
                    textView.setText(temp);
                }
            }
        });
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(start){
                    textView.setText("1");
                    start = false;
                }else {
                    String temp = textView.getText() + "1";
                    textView.setText(temp);
                }
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(start){
                    textView.setText("2");
                    start = false;
                }else {
                    String temp = textView.getText() + "2";
                    textView.setText(temp);
                }
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(start){
                    textView.setText("3");
                    start = false;
                }else {
                    String temp = textView.getText() + "3";
                    textView.setText(temp);
                }
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(start){
                    textView.setText("4");
                    start = false;
                }else {
                    String temp = textView.getText() + "4";
                    textView.setText(temp);
                }

            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(start){
                    textView.setText("5");
                    start = false;
                }else {
                    String temp = textView.getText() + "5";
                    textView.setText(temp);
                }
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(start){
                        textView.setText("6");
                        start = false;
                    }else {
                        String temp = textView.getText() + "6";
                        textView.setText(temp);
                    }
            }
        });
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(start){
                    textView.setText("7");
                    start = false;
                }else {
                    String temp = textView.getText() + "7";
                    textView.setText(temp);
                }
            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(start){
                        textView.setText("8");
                        start = false;
                    }else {
                        String temp = textView.getText() + "8";
                        textView.setText(temp);
                    }
            }
        });
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(start){
                    textView.setText("9");
                    start = false;
                }else {
                    String temp = textView.getText() + "9";
                    textView.setText(temp);
                }
            }
        });
        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                digit2 = Double.parseDouble((String) textView.getText());
                process = 1;
                textViewUp.setText(null);
                String temp = (String)textViewUp.getText() + textView.getText() + " + ";
                textViewUp.setText(temp);
                textView.setText(null);
                block();
                unBlockFraction();
            }
        });
        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                digit2 = Double.parseDouble((String) textView.getText());
                process = 2;
                textViewUp.setText(null);
                String temp = (String)textViewUp.getText() + textView.getText() + " - ";
                textViewUp.setText(temp);
                textView.setText(null);
                block();
                unBlockFraction();
            }
        });
        btnMulti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                digit2 = Double.parseDouble((String) textView.getText());
                process = 3;
                textViewUp.setText(null);
                String temp = (String)textViewUp.getText() + textView.getText() + " * ";
                textViewUp.setText(temp);
                textView.setText(null);
                block();
                unBlockFraction();
            }
        });
        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                digit2 = Double.parseDouble((String) textView.getText());
                process = 4;
                textViewUp.setText(null);
                String temp = (String)textViewUp.getText() + textView.getText() + " / ";
                textViewUp.setText(temp);
                textView.setText(null);
                block();
                unBlockFraction();
            }
        });
        btnPercent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(String.valueOf(Double.parseDouble((String) textView.getText()) / 100));
            }
        });
        btnPM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textView.getText().equals("")){
                    textView.setText("-");
                }else if(textView.getText().equals("-")){
                    textView.setText("");
                } else if(textView.getText().equals("0") ) {
                    textView.setText("0");
                } else if(textView.getText().equals("0.0")){
                    textView.setText("0.0");
                    blockFraction();
                } else {
                    double temp = Double.parseDouble((String) textView.getText());
                    temp *= -1;
                    textView.setText(String.valueOf(temp));
                    blockFraction();
                }
            }
        });
        btnFraction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp = textView.getText() + ".";
                start = false;
                textView.setText(temp);
                blockFraction();
            }
        });
        btnEquals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!textView.getText().equals("") && !textView.getText().equals("-")){
                    String temp = (String) textViewUp.getText() + textView.getText() + " =";
                    textViewUp.setText(temp);
                    start = true;
                    operation();
                    unBlock();
                    blockFraction();
                    process = 5;
                }
            }
        });
    }
    private void landscapeButtons(){
        btnSQRT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double temp = Double.parseDouble((String) textView.getText());
                String text = "SQRT( " + textView.getText() + " ) = ";
                textViewUp.setText(text);
                textView.setText(String.valueOf(Math.sqrt(temp)));
                blockFraction();
            }
        });
        btnLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = "LOG10( " + textView.getText() + " ) = ";
                        textViewUp.setText(text);
                textView.setText(String.valueOf(Math.log10(Double.parseDouble((String) textView.getText()))));
                blockFraction();
            }
        });
        btnPow2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = textView.getText() + "^2 = ";
                textViewUp.setText(text);
                textView.setText(String.valueOf(Math.pow(Double.parseDouble((String) textView.getText()), 2)));
                blockFraction();
            }
        });
        btnPow3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = textView.getText() + "^3 = ";
                textViewUp.setText(text);
                textView.setText(String.valueOf(Math.pow(Double.parseDouble((String) textView.getText()), 3)));
                blockFraction();
            }
        });
        btnSil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = textView.getText() + "! = ";
                textViewUp.setText(text);
                double temp = Double.parseDouble((String) textView.getText());
                temp = Math.floor(temp);
                double fact = 1.0;
                while(temp > 0){
                    fact *= temp;
                    temp--;
                }
                textView.setText(String.valueOf(fact));
                blockFraction();
            }
        });
    }
    private void block(){
        btnMulti.setEnabled(false);
        btnPlus.setEnabled(false);
        btnMinus.setEnabled(false);
        btnDiv.setEnabled(false);
        btnPercent.setEnabled(false);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            btnSQRT.setEnabled(false);
            btnLog.setEnabled(false);
            btnPow2.setEnabled(false);
            btnPow3.setEnabled(false);
            btnSil.setEnabled(false);
        }
    }
    private void unBlock(){
        btnMulti.setEnabled(true);
        btnPlus.setEnabled(true);
        btnMinus.setEnabled(true);
        btnDiv.setEnabled(true);
        btnPercent.setEnabled(true);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            btnSQRT.setEnabled(true);
            btnLog.setEnabled(true);
            btnPow2.setEnabled(true);
            btnPow3.setEnabled(true);
            btnSil.setEnabled(true);
        }
    }
    private void blockFraction(){
        btnFraction.setEnabled(false);
    }
    private void unBlockFraction(){
        btnFraction.setEnabled(true);
    }
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TEXT_VIEW, (String) textView.getText());
        outState.putString(TEXT_VIEW_UP, (String) textViewUp.getText());
        outState.putDouble(DIGIT, digit2);
        outState.putInt(PROCESS, process);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        String textViewSave = savedInstanceState.getString(TEXT_VIEW);
        String textViewUpSave = savedInstanceState.getString(TEXT_VIEW_UP);
        process = savedInstanceState.getInt(PROCESS);
        digit2 = savedInstanceState.getDouble(DIGIT);

        textView.setText(textViewSave);
        textViewUp.setText(textViewUpSave);
    }
}